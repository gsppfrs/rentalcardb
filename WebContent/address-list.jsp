<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Address Management Application</title>
</head>
<body>
	<div align=center>
		<h1>Address Management</h1>
		<c:if test="${roleUser==1}">
			<a href="/rentalcardb/CategoryServlet?action=list">Category</a>
			<a href="/rentalcardb/RoleServlet?action=list">Role</a>
			<a href="/rentalcardb/UserServlet?action=list">Users Management</a>
		</c:if>
		<c:if test="${roleUser==2}">
			<a href="/rentalcardb/UserServlet?action=list">My account</a>
		</c:if>
		<a href="/rentalcardb/RentalServlet?action=list">Rent</a> 
		<a href="/rentalcardb/VehicleServlet?action=list">Vehicle</a>
		<a href="/rentalcardb/UserServlet?action=logout">Logout</a>
			<c:if test="${roleUser==1}">
		<h2>
				<a href="/rentalcardb/AddressServlet?action=new">Add New Address</a>
         &nbsp;&nbsp;&nbsp;
			<a href="/rentalcardb/AddressServlet?action=list">List All Address</a> </h2>
        </c:if>
	</div>
	<div align="center">
		<table border="1" cellpadding="5">
			<caption>
				<h2>List of Addresses</h2>
			</caption>
			<tr>
				<th>ID</th>
				<th>State</th>
				<th>City</th>
				<th>Zip</th>
				<th>Street Name</th>
				<th>Civic Number</th>
				<th>Action</th>
			</tr>
			<c:if test="${roleUser==Admin}">
			<c:forEach var="address" items="${listAddress}">
				<tr>
					<td><c:out value="${address.idAddress}" /></td>
					<td><c:out value="${address.state}" /></td>
					<td><c:out value="${address.city}" /></td>
					<td><c:out value="${address.zip}" /></td>
					<td><c:out value="${address.streetName}" /></td>
					<td><c:out value="${address.civicNumber}" /></td>
					<td><a
						href="/rentalcardb/AddressServlet?action=edit&idAddress=<c:out value='${address.idAddress}' />">Edit</a>
						&nbsp;&nbsp;&nbsp;&nbsp; <a
						href="/rentalcardb/AddressServlet?action=delete&idAddress=<c:out value='${address.idAddress}' />">Delete</a>
					</td>
				</tr>
			</c:forEach>
			</c:if>
			<c:if test="${roleUser==Customer}">
			<c:forEach var="user" items="${currentUser}">
			<c:forEach var="address" items="${currentUser.addresses}">
				<tr>
					<td><c:out value="${address.idAddress}" /></td>
					<td><c:out value="${address.state}" /></td>
					<td><c:out value="${address.city}" /></td>
					<td><c:out value="${address.zip}" /></td>
					<td><c:out value="${address.streetName}" /></td>
					<td><c:out value="${address.civicNumber}" /></td>
					<td><a
						href="/rentalcardb/AddressServlet?action=edit&idAddress=<c:out value='${address.idAddress}' />">Edit</a>
					</td>
				</tr>
			</c:forEach>
			</c:forEach>
			</c:if>
		</table>
	</div>
</body>
</html>