<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
 <title>Vehicle Management Application</title>
</head>
<body>
 <div align=center>
  <h1>Vehicle Management</h1>
  <c:if test="${roleUser==1}">
			<a href="/rentalcardb/CategoryServlet?action=list">Category</a>
			<a href="/rentalcardb/RoleServlet?action=list">Role</a>
			<a href="/rentalcardb/UserServlet?action=list">Users Management</a>
		</c:if>
		<c:if test="${roleUser==2}">
			<a href="/rentalcardb/UserServlet?action=list">My account</a>
		</c:if>
		<a href="/rentalcardb/RentalServlet?action=list">Rent</a> <a
			href="/rentalcardb/VehicleServlet?action=list">Vehicle</a>
			<a href="/rentalcardb/UserServlet?action=logout">Logout</a>
		<c:if test="${roleUser==1}">
			<h2>
				<a href="/rentalcardb/VehicleServlet?action=new">Add New Vehicle</a>
				&nbsp;&nbsp;&nbsp; <a href="/rentalcardb/VehicleServlet?action=list">List
					All Vehicles</a>
			</h2>
		</c:if>
 </div>
    <div align="center">
        <table border="1" cellpadding="5">
            <caption><h2>List of Vehicles</h2></caption>
            <tr>
                <th>ID</th>
                <th>Model</th>
                <th>Year of registration</th>
                <th>License plate</th>
                <c:if test="${roleUser==1}">
                <th>Action</th>
                </c:if>
                <th>Category</th>
            </tr>
            <c:forEach var="vehicle" items="${listVehicle}">
                <tr>
                    <td><c:out value="${vehicle.idVehicle}" /></td>
                    <td><c:out value="${vehicle.model}" /></td>
                    <td><c:out value="${vehicle.yearOfRegistration}" /></td>
                    <td><c:out value="${vehicle.licensePlate}" /></td>
                    <c:if test="${roleUser==1}">
                    <td>
                     <a href="/rentalcardb/VehicleServlet?action=edit&idVehicle=<c:out value='${vehicle.idVehicle}' />">Edit</a>
                     &nbsp;&nbsp;&nbsp;&nbsp;
                     <a href="/rentalcardb/VehicleServlet?action=delete&idVehicle=<c:out value='${vehicle.idVehicle}' />">Delete</a>                  
                    </td>
                    </c:if>
                    <td><c:out value="${vehicle.category.description}" /></td>
                </tr>
            </c:forEach>
        </table>
    </div> 
</body>
</html>