<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<title>User Management Application</title>
</head>
<body>
	<div align="center">
		<h1>User Management</h1>
		<c:if test="${roleUser==1}">
			<a href="/rentalcardb/AddressServlet?action=list">Category</a>
			<a href="/rentalcardb/RoleServlet?action=list">Role</a>
			<a href="/rentalcardb/UserServlet?action=list">Users Management</a>
		</c:if>
		<c:if test="${roleUser==2}">
			<a href="/rentalcardb/UserServlet?action=list">My account</a>
		</c:if>
		<a href="/rentalcardb/RentalServlet?action=list">Rent</a> <a
			href="/rentalcardb/VehicleServlet?action=list">Vehicle</a> <a
			href="/rentalcardb/UserServlet?action=logout">Logout</a>
		<c:if test="${roleUser==1}">
			<h2>
				<a href="/rentalcardb/UserServlet?action=new">Add New User</a>
				&nbsp;&nbsp;&nbsp; <a href="/rentalcardb/UserServlet?action=list">List
					All Users</a>
			</h2>
		</c:if>
	</div>
	<div align="center">
		<c:if test="${user == null}">
			<c:set var="action" value="/rentalcardb/UserServlet?action=insert"
				scope="page" />
		</c:if>
		<c:if test="${user != null}">
			<c:set var="action" value="/rentalcardb/UserServlet?action=update"
				scope="page" />
		</c:if>
		<form action="${action}" method="post">

			<table border="1" cellpadding="5">
				<caption>
					<h2>
						<c:if test="${user != null}">
               Edit User
              </c:if>
						<c:if test="${user == null}">
               Add New User
              </c:if>
					</h2>
				</caption>
				<c:if test="${user != null}">
					<input type="hidden" name="idUser"
						value="<c:out value='${user.idUser}' />" />
				</c:if>
				<tr>
					<th>Name:</th>
					<td><input type="text" name="name" size="30" required
						value="<c:out value='${user.name}'/>" /></td>

					<th>Surname:</th>
					<td><input type="text" name="surname" size="30" required
						value="<c:out value='${user.surname}'/>" /></td>


					<th>Date of birth:</th>
					<td><input type="date" name="dateOfBirth" size="30"
						max="${eighteen}" value="${formattedDate}" /></td>
				</tr>
				<tr>
					<th>Email:</th>
					<td><input type="text" name="email" size="30" required
						value="<c:out value='${user.email}'/>" /></td>
					<th>Password:</th>
					<td><input type="text" name="password" size="30" required
						value="<c:out value='${user.password}'/>" /></td>
					<th>Type User:</th>
					<td><c:if test="${user != null}">
							<c:forEach var="role" items="${user.role}">
								<c:set var="temp" value="${role.idRole}" scope="page" />
								<c:if test="${temp==1}">
									<input type="radio" name="role" value="1" checked>
									<label for="radioRole">Admin</label>
									<input type="radio" name="role" value="2">
									<label for="radioRole">Customer</label>
								</c:if>
								<c:if test="${temp==2}">
									<input type="radio" name="role" value="1">
									<label for="radioRole">Admin</label>
									<input type="radio" name="role" value="2" checked>
									<label for="radioRole">Customer</label>
								</c:if>
							</c:forEach>
						</c:if> <c:if test="${user == null}">
							<input type="radio" name="role" value="1">
							<label for="radioRole">Admin</label>
							<input type="radio" name="role" value="2">
							<label for="radioRole">Customer</label>
						</c:if></td>
				</tr>
				<c:if test="${user != null}">
				<c:forEach var="address" items="${user.address}">
					<tr>
						<th>State:</th>
						<td><input type="text" name="state" size="30"
							value="<c:out value='${address.state}' />" /></td>
						<th>City</th>
						<td><input type="text" name="city" size="30"
							value="<c:out value='${address.city}'/>" /></td>
						<th>Zip:</th>
						<td><input type="text" name="zip" size="30"
							value="<c:out value='${address.zip}' />" /></td>
					</tr>
					<tr>
						<th>Street Name:</th>
						<td><input type="text" name="streetName" size="30"
							value="<c:out value='${address.streetName}' />" /></td>
						<th>Civic Number:</th>
						<td><input type="text" name="civicNumber" size="30"
							value="<c:out value='${address.civicNumber}' />" /></td>
					</tr>
				</c:forEach>
				</c:if>
				<c:if test="${user == null}">
					<tr>
						<th>State:</th>
						<td><input type="text" name="state" size="30"/></td>
						<th>City</th>
						<td><input type="text" name="city" size="30"/></td>
						<th>Zip:</th>
						<td><input type="text" name="zip" size="30"/></td>
					</tr>
					<tr>
						<th>Street Name:</th>
						<td><input type="text" name="streetName" size="30"/></td>
						<th>Civic Number:</th>
						<td><input type="text" name="civicNumber" size="30"/></td>
					</tr>
				</c:if>
				<tr>
					<td colspan="2" align="center"><input type="submit"
						value="Save" /></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>