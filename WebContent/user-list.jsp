<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>User Management Application</title>
</head>
<body>
	<div align=center>
		<h1>User Management</h1>
		<c:if test="${roleUser==1}">
			<a href="/rentalcardb/CategoryServlet?action=list">Category</a>
			<a href="/rentalcardb/RoleServlet?action=list">Role</a>
			<a href="/rentalcardb/UserServlet?action=list">Users Management</a>
		</c:if>
		
		<c:if test="${roleUser==2}">
			<a href="/rentalcardb/UserServlet?action=list">My account</a>
		</c:if>
		
		<a href="/rentalcardb/RentalServlet?action=list">Rent</a> <a
			href="/rentalcardb/VehicleServlet?action=list">Vehicle</a>
			<a href="/rentalcardb/UserServlet?action=logout">Logout</a>
			
		<c:if test="${roleUser==1}">
			<h2>
				<a href="/rentalcardb/UserServlet?action=new">Add New User</a>
				&nbsp;&nbsp;&nbsp; <a href="/rentalcardb/UserServlet?action=list">List
					All Users</a>
			</h2>
		</c:if>
	</div>
	<div align="center">
		<table border="1" cellpadding="5">
			<caption>
			<c:if test="${roleUser==1}">
				<h2>List of Users</h2>
			</c:if>
			<c:if test="${roleUser==2}">
				<h2>My profile</h2>
			</c:if>
			</caption>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Username</th>
				<th>Date of Birth</th>
				<th>Email</th>
				<th>Password</th>
				<th>Action</th>
				<th>Role</th>
				<th>Address</th>
			</tr>
			<c:if test="${roleUser==1}">
				<c:forEach var="user" items="${listUser}">
					<tr>
						<td><c:out value="${user.idUser}" /></td>
						<td><c:out value="${user.name}" /></td>
						<td><c:out value="${user.surname}" /></td>
						<td><c:out value="${user.dateOfBirth}" /></td>
						<td><c:out value="${user.email}" /></td>
						<td><c:out value="${user.password}" /></td>
						<td><a
							href="/rentalcardb/UserServlet?action=edit&idUser=<c:out value='${user.idUser}' />">Edit</a>
							&nbsp;&nbsp;&nbsp;&nbsp; <a
							href="/rentalcardb/UserServlet?action=delete&idUser=<c:out value='${user.idUser}' />">Delete</a>
						</td>
						<td><c:forEach var="role" items="${user.role}">
								<c:out value="${role.description}"></c:out>
							</c:forEach></td>
						<td><c:forEach var="address" items="${user.address}">
								<c:out value="${address.streetName}" />
								<c:out value="${address.civicNumber}" />
								<c:out value="${address.city}" />
								<c:out value="${address.zip}" />
								<c:out value="${address.state}" />
							</c:forEach></td>
					</tr>
				</c:forEach>
			</c:if>

			<c:if test="${roleUser==2}">
					<tr>
						<td><c:out value="${currentUser.idUser}" /></td>
						<td><c:out value="${currentUser.name}" /></td>
						<td><c:out value="${currentUser.surname}" /></td>
						<td><c:out value="${currentUser.dateOfBirth}" /></td>
						<td><c:out value="${currentUser.email}" /></td>
						<td><c:out value="${currentUser.password}" /></td>
						<td><a
							href="/rentalcardb/UserServlet?action=edit&idUser=<c:out value='${currentUser.idUser}' />">Edit</a>
						</td>
						<td><c:forEach var="role" items="${currentUser.role}">
								<c:out value="${role.description}"></c:out>
							</c:forEach></td>
						<td><c:forEach var="address" items="${currentUser.address}">
								<c:out value="${address.streetName}" />
								<c:out value="${address.civicNumber}" />
								<c:out value="${address.city}" />
								<c:out value="${address.zip}" />
								<c:out value="${address.state}" />
							</c:forEach></td>
					</tr>
			</c:if>
		</table>
	</div>
</body>
</html>