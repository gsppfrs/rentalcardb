<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Pagina principale</title>
</head>
<body>
	<div align="center">
		<h1>Benvenuto</h1>
		<c:choose>
			<c:when test="${currentUser!= null}">
				<h2>
					<c:out value="${currentUser.name}" />
					<c:out value="${currentUser.surname}" />
				</h2>
				<c:if test="${roleUser==1}">
					<a href="/rentalcardb/AddressServlet?action=list">Address</a>
					<a href="/rentalcardb/CategoryServlet?action=list">Category</a>
					<a href="/rentalcardb/RentalServlet?action=list">Rental</a>
					<a href="/rentalcardb/RoleServlet?action=list">Role</a>
					<a href="/rentalcardb/UserServlet?action=list">Users Management</a>
					<a href="/rentalcardb/VehicleServlet?action=list">Vehicle</a>
					<a href="/rentalcardb/UserServlet?action=logout">Logout</a>
				</c:if>
				<c:if test="${roleUser==2}">
					<a href="/rentalcardb/RentalServlet?action=list">My Rent</a>
					<a href="/rentalcardb/UserServlet?action=list">My Account</a>
					<a href="/rentalcardb/VehicleServlet?action=list">Vehicle</a>
					<a href="/rentalcardb/UserServlet?action=logout">Logout</a>
				</c:if>
			</c:when>
			<c:otherwise>
				<a href="/rentalcardb/login.jsp">Login</a>
			</c:otherwise>
		</c:choose>
	</div>
</body>
</html>