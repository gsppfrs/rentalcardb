<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
 <title>Rental Management Application</title>
</head>
<body>
 <div align=center>
  <h1>Rental Management</h1>
  <c:if test="${roleUser==1}">
			<a href="/rentalcardb/CategoryServlet?action=list">Category</a>
			<a href="/rentalcardb/RoleServlet?action=list">Role</a>
			<a href="/rentalcardb/UserServlet?action=list">Users Management</a>
		</c:if>
		<c:if test="${roleUser==2}">
			<a href="/rentalcardb/UserServlet?action=list">My account</a>
		</c:if>
		<a href="/rentalcardb/RentalServlet?action=list">Rent</a> <a
			href="/rentalcardb/VehicleServlet?action=list">Vehicle</a>
			<a href="/rentalcardb/UserServlet?action=logout">Logout</a>
		
			<h2>
				<a href="/rentalcardb/RentalServlet?action=new">Add New Rent</a>
				&nbsp;&nbsp;&nbsp;
				<a href="/rentalcardb/RentalServlet?action=list">
					<c:if test="${roleUser==1}">List All Rent</c:if>
					<c:if test="${roleUser==2}">My Rent</c:if>
				</a>
			</h2>
 </div>
    <div align="center">
        <table border="1" cellpadding="5">
            <caption>
	            <h2>List of Rentals</h2>
	            
	        </caption>
            <tr>
                <th>ID</th>
                <th>Rental Start Date</th>
                <th>Rental End Date</th>
                <th>ID Vehicle</th>
                <c:if test="${roleUser==1}">
                <th>Id User</th>
                <th>Action</th>
                </c:if>
                <th>Validate</th>
            </tr>
            <c:forEach var="rental" items="${listRentals}">
                <tr>
                    <td><c:out value="${rental.idRental}" /></td>
                    <td><c:out value="${rental.rentalStartDate}" /></td>
                    <td><c:out value="${rental.rentalEndDate}" /></td>
                    <td><c:out value="${rental.vehicle.idVehicle}"/></td>
                    <c:if test="${roleUser==1}">
                    <td><c:out value="${rental.user.idUser}"/></td>
                    </c:if>
                    <c:if test="${rental.validate==0}">
                    <td><c:out value="Request send" /></td>
                    </c:if>
                    <c:if test="${rental.validate==1}">
                    <td><c:out value="Request proved" /></td>
                    </c:if>
                    <c:if test="${rental.validate==-1}">
                    <td><c:out value="Request denied" /></td>
                    </c:if>
                    <c:if test="${roleUser==1}">
                    <td>
                     <a href="/rentalcardb/RentalServlet?action=edit&idRental=<c:out value='${rental.idRental}' />">Edit</a>
                     &nbsp;&nbsp;&nbsp;&nbsp;
                     <a href="/rentalcardb/RentalServlet?action=delete&idRental=<c:out value='${rental.idRental}' />">Delete</a>                  
                    </td>
                    </c:if>
                </tr>
            </c:forEach>
            </table>
            <c:if test="${roleUser==2}">
            <table border="1" cellpadding="5">
			<h2>My Rental</h2>            
            <c:forEach var="rental" items="${listRentals}">
            	<c:if test="${rental.user.idUser==currentUser.idUser}">
                <tr>
                    <td><c:out value="${rental.idRental}" /></td>
                    <td><c:out value="${rental.rentalStartDate}" /></td>
                    <td><c:out value="${rental.rentalEndDate}" /></td>
                    <td><c:out value="${rental.vehicle.idVehicle}"/></td>
                    <td><c:out value="${rental.user.idUser}"/></td>
                    <c:if test="${rental.validate==0}">
                    <td><c:out value="Request send" /></td>
                    </c:if>
                    <c:if test="${rental.validate==1}">
                    <td><c:out value="Request proved" /></td>
                    </c:if>
                    <c:if test="${rental.validate==-1}">
                    <td><c:out value="Request denied" /></td>
                    </c:if>
                    <c:if test="${rental.rentalStartDate>plusTwoDays}">
                    <td>
                     <a href="/rentalcardb/RentalServlet?action=edit&idRental=<c:out value='${rental.idRental}' />">Edit</a>
                     &nbsp;&nbsp;&nbsp;&nbsp;
                     <a href="/rentalcardb/RentalServlet?action=delete&idRental=<c:out value='${rental.idRental}' />">Delete</a>                  
                    </td>
                    </c:if>
                </tr>
                </c:if>
            </c:forEach>
        </table>
            </c:if>
    </div> 
</body>
</html>