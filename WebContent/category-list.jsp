<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
 <title>Category Management Application</title>
</head>
<body>
 <div align=center>
  <h1>Category Management</h1>
  <c:if test="${roleUser==1}">
			<a href="/rentalcardb/CategoryServlet?action=list">Category</a>
			<a href="/rentalcardb/RoleServlet?action=list">Role</a>
			<a href="/rentalcardb/UserServlet?action=list">Users Management</a>
		</c:if>
		<c:if test="${roleUser==2}">
			<a href="/rentalcardb/UserServlet?action=list">My account</a>
		</c:if>
		<a href="/rentalcardb/RentalServlet?action=list">Rent</a> <a
			href="/rentalcardb/VehicleServlet?action=list">Vehicle</a>
			<a href="/rentalcardb/UserServlet?action=logout">Logout</a>
		<c:if test="${roleUser==1}">
			<h2>
				<a href="/rentalcardb/CategoryServlet?action=new">Add New Category</a>
				&nbsp;&nbsp;&nbsp; <a href="/rentalcardb/CategoryServlet?action=list">List
					All Categories</a>
			</h2>
		</c:if>
 </div>
    <div align="center">
        <table border="1" cellpadding="5">
            <caption><h2>List of Categories</h2></caption>
            <tr>
                <th>ID</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
            <c:forEach var="category" items="${listCategories}">
                <tr>
                    <td><c:out value="${category.idCategory}" /></td>
                    <td><c:out value="${category.description}" /></td>
                    <td>
                     <a href="/rentalcardb/CategoryServlet?action=edit&idCategory=<c:out value='${category.idCategory}' />">Edit</a>
                     &nbsp;&nbsp;&nbsp;&nbsp;
                     <a href="/rentalcardb/CategoryServlet?action=delete&idCategory=<c:out value='${category.idCategory}' />">Delete</a>                  
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div> 
</body>
</html>