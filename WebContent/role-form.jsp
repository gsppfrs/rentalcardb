<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Role Management Application</title>
</head>
<body>
	<div align="center">
		<h1>Role Management</h1>
		<c:if test="${roleUser==1}">
			<a href="/rentalcardb/CategoryServlet?action=list">Category</a>
			<a href="/rentalcardb/RoleServlet?action=list">Role</a>
			<a href="/rentalcardb/UserServlet?action=list">Users Management</a>
		</c:if>
		<c:if test="${roleUser==2}">
			<a href="/rentalcardb/UserServlet?action=list">My account</a>
		</c:if>
		<a href="/rentalcardb/RentalServlet?action=list">Rent</a> <a
			href="/rentalcardb/VehicleServlet?action=list">Vehicle</a>
			<a href="/rentalcardb/UserServlet?action=logout">Logout</a>
		<c:if test="${roleUser==1}">
			<h2>
				<a href="/rentalcardb/RoleServlet?action=new">Add New Role</a>
				&nbsp;&nbsp;&nbsp; <a href="/rentalcardb/RoleServlet?action=list">List
					All Roles</a>
			</h2>
		</c:if>
	</div>
	<div align="center">
		<c:if test="${role == null}">
			<c:set var="action" value="/rentalcardb/RoleServlet?action=insert" scope="page" />
			</c:if>
			<c:if test="${role != null}">
			<c:set var="action" value="/rentalcardb/RoleServlet?action=update" scope="page" />
			</c:if>
				<form action="${action}" method="post">
			<table border="1" cellpadding="5">
				<caption>
					<h2>
						<c:if test="${role != null}">
               Edit Role
              </c:if>
						<c:if test="${role == null}">
               Add New Role
              </c:if>
					</h2>
				</caption>
				<c:if test="${role != null}">
					<input type="hidden" name="idRole"
						value="<c:out value='${role.idRole}' />" />
				</c:if>
				<tr>
					<th>Description:</th>
					<td><input type="text" name="description" size="30"
						value="<c:out value='${role.description}' />" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit" value="Save" /></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>