<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Rental Management Application</title>
</head>
<body>
	<div align="center">
		<h1>Rental Management</h1>
		<c:if test="${roleUser==1}">
			<a href="/rentalcardb/CategoryServlet?action=list">Category</a>
			<a href="/rentalcardb/RoleServlet?action=list">Role</a>
			<a href="/rentalcardb/UserServlet?action=list">Users Management</a>
		</c:if>
		<c:if test="${roleUser==2}">
			<a href="/rentalcardb/UserServlet?action=list">My account</a>
		</c:if>
		<a href="/rentalcardb/RentalServlet?action=list">Rent</a> <a
			href="/rentalcardb/VehicleServlet?action=list">Vehicle</a>
			<a href="/rentalcardb/UserServlet?action=logout">Logout</a>
		<c:if test="${roleUser==1}">
			<h2>
				<a href="/rentalcardb/RentalServlet?action=new">Add New Rent</a>
				&nbsp;&nbsp;&nbsp; <a href="/rentalcardb/RentalServlet?action=list">List
					All Rent</a>
			</h2>
		</c:if>
		<c:if test="${roleUser==2}">
			<h2>
				<a href="/rentalcardb/RentalServlet?action=new">Add New Rent</a>
				&nbsp;&nbsp;&nbsp; <a href="/rentalcardb/RentalServlet?action=list">My Rent</a>
			</h2>
		</c:if>
	</div>
	<div align="center">
		<c:if test="${rental == null}">
			<c:set var="action" value="/rentalcardb/RentalServlet?action=insert" scope="page" />
			</c:if>
			<c:if test="${rental != null}">
			<c:set var="action" value="/rentalcardb/RentalServlet?action=update" scope="page" />
			</c:if>
				<form action="${action}" method="post">
			<table border="1" cellpadding="5">
				<caption>
					<h2>
						<c:if test="${rental != null}">
               Edit Rental
              </c:if>
						<c:if test="${rental == null}">
               Add New Rental
              </c:if>
					</h2>
				</caption>
				<c:if test="${rental != null}">
					<input type="hidden" name="idRental"
						value="<c:out value='${rental.idRental}' />" />
				</c:if>
				<tr>
					<th>Rental Start Date:</th>
					<c:if test="${roleUser==1}">
					<td><input type="date" name="rentalStartDate" size="30"
						value="<c:out value='${formattedStartDate}' />" /></td>
					<th>Rental End Date:</th>
					<td><input type="date" name="rentalEndDate" size="30"
						value="<c:out value='${formattedEndDate}' />" /></td>
					</c:if>
					<c:if test="${roleUser==2}">
					<td><input type="date" name="rentalStartDate" size="30" min="${currentDate}"
						value="<c:out value='${formattedStartDate}' />" /></td>
					<th>Rental End Date:</th>
					<td><input type="date" name="rentalEndDate" size="30" min="${currentDate}"
						value="<c:out value='${formattedEndDate}' />" /></td>
					</c:if>
					<th>Id Vehicle:</th>
					<td><input type="text" name="idVehicle" size="30"
						value="<c:out value='${rental.vehicle.idVehicle}' />"/></td>
						<c:if test="${rental != null}">
							<input type="hidden" name="idUser"
							value="<c:out value='${rental.user.idUser}' />"/>
						</c:if>
						<c:if test="${rental == null}">
							<input type="hidden" name="idUser"
							value="<c:out value='${currentUser.idUser}' />"/>
						</c:if>
					<c:if test="${roleUser==1}">
					<th>Validation:</th>
					<td>
						<input type="radio" name="validate" value="1">
						<label for="radioValidate">Approve</label>
						<input type="radio" name="validate" value="-1">
						<label for="radioValidate">Don't approve</label>
						<input type="radio" name="validate" value="0" checked>
						<label for="radioValidate">Pending</label>
					</td>
					</c:if>
					<c:if test="${roleUser==2}">
					<th>Validation:</th>
					<td>
						<label for="radioValidate">Pending</label>
						<input type="checkbox" name="validate" value="0" checked>
					</td>
					</c:if>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit" value="Save" /></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>