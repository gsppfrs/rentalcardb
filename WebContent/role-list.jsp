<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
 <title>Role Management Application</title>
</head>
<body>
 <div align = center>
  <h1>Role Management</h1>
  <c:if test="${roleUser==1}">
			<a href="/rentalcardb/CategoryServlet?action=list">Category</a>
			<a href="/rentalcardb/RoleServlet?action=list">Role</a>
			<a href="/rentalcardb/UserServlet?action=list">Users Management</a>
		</c:if>
		<c:if test="${roleUser==2}">
			<a href="/rentalcardb/UserServlet?action=list">My account</a>
		</c:if>
		<a href="/rentalcardb/RentalServlet?action=list">Rent</a> <a
			href="/rentalcardb/VehicleServlet?action=list">Vehicle</a>
			<a href="/rentalcardb/UserServlet?action=logout">Logout</a>
		<c:if test="${roleUser==1}">
			<h2>
				<a href="/rentalcardb/RoleServlet?action=new">Add New Role</a>
				&nbsp;&nbsp;&nbsp; <a href="/rentalcardb/RoleServlet?action=list">List
					All Roles</a>
			</h2>
		</c:if>
 </div>
    <div align="center">
        <table border="1" cellpadding="5">
            <caption><h2>List of Roles</h2></caption>
            <tr>
                <th>ID</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
            <c:forEach var="role" items="${listRole}">
                <tr>
                    <td><c:out value="${role.idRole}" /></td>
                    <td><c:out value="${role.description}" /></td>
                    <td>
                     <a href="/rentalcardb/RoleServlet?action=edit&idRole=<c:out value='${role.idRole}' />">Edit</a>
                     &nbsp;&nbsp;&nbsp;&nbsp;
                     <a href="/rentalcardb/RoleServlet?action=delete&idRole=<c:out value='${role.idRole}' />">Delete</a>                  
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div> 
</body>
</html>