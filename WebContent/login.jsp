<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

 <div align = "center">
  <h1>Login Form</h1>
  <form action="/rentalcardb/UserServlet?action=login" method="post">
   <div class="form-group">
    <label for="login">Email:</label> <input type="text"
     class="form-control" id="email" placeholder="Email"
     name="email" required>
   </div>
   <div class="form-group">
    <label for="login">Password:</label> <input type="password"
     class="form-control" id="password" placeholder="Password"
     name="password" required>
   </div>
   <button type="submit" class="btn btn-primary">Submit</button>
  </form>
 </div>
</body>
</html>