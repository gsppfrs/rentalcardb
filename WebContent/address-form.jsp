<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Address Management Application</title>
</head>
<body>
	<div align="center">
	
		<h1>Address Management</h1>
		<c:if test="${roleUser==1}">
			<a href="/rentalcardb/CategoryServlet?action=list">Category</a>
			<a href="/rentalcardb/RoleServlet?action=list">Role</a>
			<a href="/rentalcardb/UserServlet?action=list">Users Management</a>
		</c:if>
		<c:if test="${roleUser==2}">
			<a href="/rentalcardb/UserServlet?action=list">My account</a>
		</c:if>
		<a href="/rentalcardb/RentalServlet?action=list">Rent</a> 
		<a href="/rentalcardb/VehicleServlet?action=list">Vehicle</a>
		<a href="/rentalcardb/UserServlet?action=logout">Logout</a>
			<c:if test="${roleUser==1}">
		<h2>
				<a href="/rentalcardb/AddressServlet?action=new">Add New Address</a>
         &nbsp;&nbsp;&nbsp;
			<a href="/rentalcardb/AddressServlet?action=list">List All Address</a> </h2>
        </c:if>
	</div>
	<div align="center">
		<c:if test="${address == null}">
			<c:set var="action" value="/rentalcardb/AddressServlet?action=insert" scope="page" />
			</c:if>
			<c:if test="${address != null}">
			<c:set var="action" value="/rentalcardb/AddressServlet?action=update" scope="page" />
			</c:if>
				<form action="${action}" method="post">
			<table border="1" cellpadding="5">
				<caption>
					<h2>
						<c:if test="${address != null}">
               Edit Address
              </c:if>
						<c:if test="${address == null}">
               Add New Address
              </c:if>
					</h2>
				</caption>
				<c:if test="${address != null}">
					<input type="hidden" name="idAddress"
						value="<c:out value='${address.idAddress}' />" />
				</c:if>
				<tr>
					<th>State:</th>
					<td><input type="text" name="state" size="30"
						value="<c:out value='${address.state}' />" /></td>
					<th>City</th>
					<td><input type="text" name="city" size="30"
						value="<c:out value='${address.city}'/>" /></td>
					<th>Zip:</th>
					<td><input type="text" name="zip" size="30"
						value="<c:out value='${address.zip}' />" /></td>
					<th>Street Name:</th>
					<td><input type="text" name="streetName" size="30"
						value="<c:out value='${address.streetName}' />" /></td>
					<th>Civic Number:</th>
					<td><input type="text" name="civicNumber" size="30"
						value="<c:out value='${address.civicNumber}' />" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit" value="Save" /></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>