<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Vehicle Management Application</title>
</head>
<body>
	<div align="center">
		<h1>Vehicle Management</h1>
		<c:if test="${roleUser==1}">
			<a href="/rentalcardb/CategoryServlet?action=list">Category</a>
			<a href="/rentalcardb/RoleServlet?action=list">Role</a>
			<a href="/rentalcardb/UserServlet?action=list">Users Management</a>
		</c:if>
		<c:if test="${roleUser==2}">
			<a href="/rentalcardb/UserServlet?action=list">My account</a>
		</c:if>
		<a href="/rentalcardb/RentalServlet?action=list">Rent</a> <a
			href="/rentalcardb/VehicleServlet?action=list">Vehicle</a>
			<a href="/rentalcardb/UserServlet?action=logout">Logout</a>
		<c:if test="${roleUser==1}">
			<h2>
				<a href="/rentalcardb/VehicleServlet?action=new">Add New Vehicle</a>
				&nbsp;&nbsp;&nbsp; <a href="/rentalcardb/VehicleServlet?action=list">List
					All Vehicles</a>
			</h2>
		</c:if>
	</div>
	<div align="center">
		<c:if test="${vehicle == null}">
			<c:set var="action" value="/rentalcardb/VehicleServlet?action=insert" scope="page" />
			</c:if>
			<c:if test="${vehicle != null}">
			<c:set var="action" value="/rentalcardb/VehicleServlet?action=update" scope="page" />
			</c:if>
				<form action="${action}" method="post">
			<table border="1" cellpadding="5">
				<caption>
					<h2>
						<c:if test="${vehicle != null}">
               Edit Vehicle
              </c:if>
						<c:if test="${vehicle == null}">
               Add New Vehicle
              </c:if>
					</h2>
				</caption>
				<c:if test="${vehicle != null}">
					<input type="hidden" name="idVehicle"
						value="<c:out value='${vehicle.idVehicle}' />" />
				</c:if>
				<tr>
					<th>Model:</th>
					<td><input type="text" name="model" size="30"
						value="<c:out value='${vehicle.model}' />" /></td>
					<th>Year of registration</th>
					<td><input type="date" name="yearOfRegistration" size="30" max="${currentDate}"
						value="<c:out value='${formattedDate}'/>" /></td>
					<th>License plate:</th>
					<td><input type="text" name="licensePlate" size="30"
						value="<c:out value='${vehicle.licensePlate}' />" /></td>
					<th>Id Category:</th>
					<td><input type="text" name="idCategory" size="30"
						value="<c:out value='${vehicle.category.idCategory}' />" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit" value="Save" /></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>