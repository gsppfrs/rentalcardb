package com.si2001.web.jdbc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.si2001.web.jdbc.model.Vehicle;
import com.si2001.web.jdbc.util.HibernateUtil;

public class VehicleDao {

	/**
     * Save Vehicle
     * @param vechile
     */
    public void saveVehicle(Vehicle vehicle) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the vehicle object
            session.save(vehicle);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    /**
     * Update Vehicle
     * @param vehicle
     */
    public void updateVehicle(Vehicle vehicle) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the vehicle object
            session.update(vehicle);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    /**
     * Delete Vehicle
     * @param idVehicle
     */
    public void deleteVehicle(Integer idVehicle) {

        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();

            // Delete a vehicle object
            Vehicle vehicle = session.get(Vehicle.class, idVehicle);
            if (vehicle != null) {
                session.delete(vehicle);
                System.out.println("vehicle is deleted");
            }

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    /**
     * Get Vehicle By ID
     * @param idVehicle
     * @return
     */
    public Vehicle getVehicle(Integer idVehicle) {

        Transaction transaction = null;
        Vehicle vehicle = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an vehicle object
            vehicle = session.get(Vehicle.class, idVehicle);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return vehicle;
    }

    /**
     * Get all Vehicles
     * @return
     */
	@SuppressWarnings("unchecked")
	public List < Vehicle > getAllVehicles() {

        Transaction transaction = null;
        List <Vehicle> listOfVehicles = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an vehicle object
            String queryString = "from Vehicle";
            listOfVehicles = session.createQuery(queryString).getResultList();
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return listOfVehicles;
    }
}