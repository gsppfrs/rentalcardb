package com.si2001.web.jdbc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.si2001.web.jdbc.model.Address;
import com.si2001.web.jdbc.util.HibernateUtil;

/**
 * CRUD database operations
 *
 */
public class AddressDao {

	/**
	 * Save Address
	 * @param address
	 */
	public void saveAddress(Address address) {
		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();
			// save the address object
			session.save(address);
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	/**
	 * Update Address
	 * @param address
	 */
	public void updateAddress(Address address) {
		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();
			// save the address object
			session.update(address);
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	/**
	 * Delete Address
	 * @param idAddress
	 */
	public void deleteAddress(int idAddress) {

		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();

			// Delete a address object
			Address address = session.get(Address.class, idAddress);
			if (address != null) {
				session.delete(address);
				System.out.println("address is deleted");
			}

			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	/**
	 * Get Address By ID
	 * @param idAddress
	 * @return
	 */
	public Address getAddress(int idAddress) {

		Transaction transaction = null;
		Address address = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();
			// get an address object
			address = session.get(Address.class, idAddress);
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return address;
	}


	/**
	 * Get Address By All except ID
	 * @param idAddress
	 * @return
	 */
	public Address getAddressWithOutId(String state, String city, Integer zip, String streetName, Integer civicNumber) {

		Transaction transaction = null;
		Address address = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();
			// get an address object


			address = (Address) session.createQuery(" from Address WHERE city = :checkCity AND civicNumber = :checkCivicNumber"
					+ " AND state = :checkState"
					+ " AND streetName = :checkStreetName AND zip = :checkZip")
					.setParameter("checkState", state).setParameter("checkCity", city).setParameter("checkStreetName", streetName)
					.setParameter("checkCivicNumber", civicNumber).setParameter("checkZip", zip).getSingleResult();

			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return address;
	}





	/**
	 * Get all Addresses
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List < Address > getAllAddress() {

		Transaction transaction = null;
		List <Address> listOfAddress = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();
			// get an address object
			String queryString = "from Address";
			listOfAddress = session.createQuery(queryString).getResultList();
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return listOfAddress;
	}
}