package com.si2001.web.jdbc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.si2001.web.jdbc.model.Category;
import com.si2001.web.jdbc.util.HibernateUtil;

public class CategoryDao {

	/**
     * Save category
     * @param category
     */
    public void saveCategory(Category category) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the category object
            session.save(category);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    /**
     * Update category
     * @param category
     */
    public void updateCategory(Category category) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the category object
            session.update(category);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    /**
     * Delete category
     * @param idCategory
     */
    public void deleteCategory(int idCategory) {

        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();

            // Delete a category object
            Category category = session.get(Category.class, idCategory);
            if (category != null) {
                session.delete(category);
                System.out.println("category is deleted");
            }

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    /**
     * Get Category By ID
     * @param idCategory
     * @return
     */
    public Category getCategory(Integer idCategory) {

        Transaction transaction = null;
        Category category = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an category object
            category = session.get(Category.class, idCategory);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return category;
    }

    /**
     * Get all Categories
     * @return
     */
	@SuppressWarnings("unchecked")
	public List < Category > getAllCategory() {

        Transaction transaction = null;
        List <Category> listOfCategories = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an category object
            String queryString = "from Category";
            listOfCategories = session.createQuery(queryString).getResultList();
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return listOfCategories;
    }
}