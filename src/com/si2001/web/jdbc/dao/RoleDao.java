package com.si2001.web.jdbc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.si2001.web.jdbc.model.Role;
import com.si2001.web.jdbc.util.HibernateUtil;

public class RoleDao {

	/**
     * Save Role
     * @param Role
     */
    public void saveRole(Role role) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the role object
            session.save(role);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    /**
     * Update Role
     * @param role
     */
    public void updateRole(Role role) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the role object
            session.update(role);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    /**
     * Delete Role
     * @param idRole
     */
    public void deleteRole(Integer idRole) {

        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();

            // Delete a role object
            Role role = session.get(Role.class, idRole);
            if (role != null) {
                session.delete(role);
                System.out.println("role is deleted");
            }

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    /**
     * Get Role By ID
     * @param idRole
     * @return
     */
    public Role getRole (Integer idRole) {

        Transaction transaction = null;
        Role role = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an role object
            role = session.get(Role.class, idRole);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return role;
    }

    /**
     * Get all Roles
     * @return
     */
	@SuppressWarnings("unchecked")
	public List < Role > getAllRoles() {

        Transaction transaction = null;
        List <Role> listOfRoles = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an role object
            String queryString = "from Role";
            listOfRoles = session.createQuery(queryString).getResultList();
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return listOfRoles;
    }
}
