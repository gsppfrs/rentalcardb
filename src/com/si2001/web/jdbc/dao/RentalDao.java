package com.si2001.web.jdbc.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.Transaction;
import com.si2001.web.jdbc.model.Rental;
import com.si2001.web.jdbc.util.HibernateUtil;

public class RentalDao {

	/**
	 * Save Rental
	 * @param rental
	 */
	public void saveRental(Rental rental) {
		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();
			// save the rental object
			session.save(rental);
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	/**
	 * Update Rental
	 * @param rental
	 */
	public void updateRental(Rental rental) {
		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();
			// save the rental object
			session.update(rental);
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	/**
	 * Delete Rental
	 * @param idRental
	 */
	public void deleteRental(Integer idRental) {

		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();

			// Delete a rental object
			Rental rental = session.get(Rental.class, idRental);
			if (rental != null) {
				session.delete(rental);
				System.out.println("rental is deleted");
			}

			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	/**
	 * Get Rental By ID
	 * @param idRental
	 * @return
	 */
	public Rental getRental(Integer idRental) {

		Transaction transaction = null;
		Rental rental = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();
			// get an rental object
			rental = session.get(Rental.class, idRental);
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return rental;
	}

	/**
	 * Get all Rentals
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List < Rental > getAllRentals() {

		Transaction transaction = null;
		List <Rental> listOfRental = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();
			// get an rental object
			String queryString = "from Rental";
			listOfRental = session.createQuery(queryString).getResultList();
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return listOfRental;
	}

	@SuppressWarnings("unchecked")
	public List<Rental> checkDate(Rental newRental) {

		Transaction transaction = null;
		List <Rental> listOfRental = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();
			// get an rental object
			
			TypedQuery<Rental> query = session.createQuery(" from Rental");

			listOfRental = query.getResultList();
			// commit transaction
			transaction.commit();
		} catch (Exception e) {

			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return listOfRental;
	}
}