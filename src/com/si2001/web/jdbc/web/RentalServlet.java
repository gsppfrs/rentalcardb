package com.si2001.web.jdbc.web;

import java.io.IOException;
import java.util.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.si2001.web.jdbc.model.Rental;
import com.si2001.web.jdbc.service.RentalService;

/**
 * Servlet implementation class RentalServlet
 */
@WebServlet("/RentalServlet")
public class RentalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private RentalService rentalService;

    public void init() {
    	rentalService = new RentalService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String action = request.getParameter("action");
        String idRental = request.getParameter("idRental");
        RequestDispatcher dispatcher = null;
        
        try {
            switch (action) {
                case "new":
                	dispatcher = request.getRequestDispatcher("rental-form.jsp");
                	dispatcher.forward(request, response);
                	break;
                case "insert":
                	      	
            		String newTempStartDate = request.getParameter("rentalStartDate");
                	Date newRentalStartDate = new SimpleDateFormat("yyyy-MM-dd").parse(newTempStartDate);
                	String newTempEndDate = request.getParameter("rentalEndDate");
                	Date newRentalEndDate = new SimpleDateFormat("yyyy-MM-dd").parse(newTempEndDate);
                	String idVehicle = request.getParameter("idVehicle");
                	String idUser = request.getParameter("idUser");
                	Integer newValidate = Integer.parseInt(request.getParameter("validate"));
            		Rental newRental = new Rental(newRentalStartDate, newRentalEndDate, newValidate);
            		rentalService.insertRental(newRental, idVehicle, idUser);
                    response.sendRedirect("/rentalcardb/RentalServlet?action=list");                    	
                    break;
                case "delete":
                	rentalService.deleteRental(idRental);
                	response.sendRedirect("/rentalcardb/RentalServlet?action=list");
                    break;
                case "edit":
                	Rental temRental = rentalService.getRentalById(Integer.parseInt(idRental));
                	String formattedStartDate = new SimpleDateFormat("yyyy-MM-dd").format(temRental.getRentalStartDate());
                	request.setAttribute("formattedStartDate", formattedStartDate);
                	String formattedEndDate = new SimpleDateFormat("yyyy-MM-dd").format(temRental.getRentalStartDate());
                	request.setAttribute("formattedEndDate", formattedEndDate);
                	request.setAttribute("rental", rentalService.getRentalById(Integer.parseInt(idRental)));
                	dispatcher = request.getRequestDispatcher("rental-form.jsp");
                	dispatcher.forward(request, response);
                    break;
                case "update":
                	
                	String tempStartDate = request.getParameter("rentalStartDate");
                	Date rentalStartDate = new SimpleDateFormat("yyyy-MM-dd").parse(tempStartDate);
                	String tempEndDate = request.getParameter("rentalEndDate");
                	Date rentalEndDate = new SimpleDateFormat("yyyy-MM-dd").parse(tempEndDate);
                	String idUserString = request.getParameter("idUser");
                	String idVehicleString = request.getParameter("idVehicle");
                	Integer validate = Integer.parseInt(request.getParameter("validate"));
                	rentalService.updateRental(idRental, rentalStartDate, rentalEndDate, validate, idUserString, idVehicleString);
                	response.sendRedirect("/rentalcardb/RentalServlet?action=list");
                    break;
                default:
                	request.setAttribute("listRentals", rentalService.listRentals());
                	dispatcher = request.getRequestDispatcher("rental-list.jsp");
                	dispatcher.forward(request, response);
                    break;
            }
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            try {
            	request.setAttribute("listRentals", rentalService.listRentals());
            	dispatcher = request.getRequestDispatcher("rental-list.jsp");
            	dispatcher.forward(request, response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
}
