package com.si2001.web.jdbc.web;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.si2001.web.jdbc.model.Category;
import com.si2001.web.jdbc.service.CategoryService;

/**
 * Servlet implementation class CategoryServlet
 */
@WebServlet("/CategoryServlet")
public class CategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private CategoryService categoryService;

    public void init() {
    	categoryService = new CategoryService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String action = request.getParameter("action");
        String idCategory = request.getParameter("idCategory");
        RequestDispatcher dispatcher = null;
        
        try {
            switch (action) {
                case "new":
                	dispatcher = request.getRequestDispatcher("category-form.jsp");
                	dispatcher.forward(request, response);
                	break;
                case "insert":
                	String newDescription = request.getParameter("description");
            		Category newCategory = new Category(newDescription);
                    categoryService.insertCategory(newCategory);
                    response.sendRedirect("/rentalcardb/CategoryServlet?action=list");
                    break;
                case "delete":
                	categoryService.deleteCategory(idCategory);
                	response.sendRedirect("/rentalcardb/CategoryServlet?action=list");
                    break;
                case "edit":
                	request.setAttribute("category", categoryService.showEditForm(Integer.parseInt(idCategory)));
                	dispatcher = request.getRequestDispatcher("category-form.jsp");
                	dispatcher.forward(request, response);
                    break;
                case "update":
                	String description = request.getParameter("description");
            		Category category = new Category(Integer.parseInt(idCategory), description);
                	categoryService.updateCategory(category);
                	response.sendRedirect("/rentalcardb/CategoryServlet?action=list");
                    break;
                default:
                	request.setAttribute("listCategories", categoryService.listCategories());
                	dispatcher = request.getRequestDispatcher("category-list.jsp");
                	dispatcher.forward(request, response);
                    break;
            }
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            try {
            	request.setAttribute("listCategories", categoryService.listCategories());
            	dispatcher = request.getRequestDispatcher("category-list.jsp");
            	dispatcher.forward(request, response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
}