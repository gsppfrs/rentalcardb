package com.si2001.web.jdbc.web;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.si2001.web.jdbc.dao.UserDao;
import com.si2001.web.jdbc.model.Address;
import com.si2001.web.jdbc.model.Role;
import com.si2001.web.jdbc.model.User;
import com.si2001.web.jdbc.service.AddressService;
import com.si2001.web.jdbc.service.UserService;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private UserService userService;
    private AddressService addressService;
    public void init() {
        userService = new UserService();
        addressService = new AddressService();
        new UserDao();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    	HttpSession session = request.getSession();
        String action = request.getParameter("action");
        String idUser = request.getParameter("idUser");
        RequestDispatcher dispatcher = null;
        
        try {
            switch (action) {
	            case "login":
	            	String loginEmail = request.getParameter("email");
	            	String loginPassword = request.getParameter("password");
	            	if(UserService.checkLogin(loginEmail, loginPassword)) {
	            		User user = userService.getUserByEmail(loginEmail);
	            		session.setAttribute("currentUser", user);
	            		for(Role temp: user.getRole()) {
	            			int i = temp.getIdRole();
	            			if(i == 1) {
	            				session.setAttribute("roleUser", i);
	            				break;
	            			}else if(i == 2){
	            				session.setAttribute("roleUser", i);
							}
	            		}
	            		
	            		ZoneId defaultZoneId = ZoneId.systemDefault();
	            		
	            		LocalDate dateLess18yyyy = LocalDate.now().minusYears(18);
	            		Date date = Date.from(dateLess18yyyy.atStartOfDay(defaultZoneId).toInstant());
	            		String currentDateLess18yyyy = new SimpleDateFormat("yyyy-MM-dd").format(date);
	            		session.setAttribute("eighteen", currentDateLess18yyyy);
	            		
	            		LocalDate datePlusTwoDays = LocalDate.now().plusDays(2);
	            		Date datePlusTwo = Date.from(datePlusTwoDays.atStartOfDay(defaultZoneId).toInstant());
	            		String currentDatePlusTwoDays = new SimpleDateFormat("yyyy-MM-dd").format(datePlusTwo);
	            		session.setAttribute("plusTwoDays", currentDatePlusTwoDays);
	            		
	            		String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(System.currentTimeMillis());
	                	session.setAttribute("currentDate", currentDate);
	            		response.sendRedirect("/rentalcardb/index.jsp");
	            	}else {
	            		dispatcher = request.getRequestDispatcher("login.jsp");
	                	dispatcher.forward(request, response);
					}
	            	break;
                case "new":
                	dispatcher = request.getRequestDispatcher("user-form.jsp");
                	dispatcher.forward(request, response);
                	break;
                case "insert":
                	String newName = request.getParameter("name");
            		String newSurname = request.getParameter("surname");
            		
            		String newTempDate = request.getParameter("dateOfBirth");
            		Date newDateOfBirth = new SimpleDateFormat("yyyy-MM-dd").parse(newTempDate);      
            		
            		String newEmail = request.getParameter("email");
            		String newPassword = request.getParameter("password");
            		String roleNewUser = request.getParameter("role");
            		
            		
            		
            		String addressState = request.getParameter("state");
            		String addressCity = request.getParameter("city");
            		String addressZip = request.getParameter("zip");
            		String addressStreet = request.getParameter("streetName");
            		String addressCivic = request.getParameter("civicNumber");

            		
            		User newUser = new User(newName, newSurname, newDateOfBirth, newEmail,newPassword);
                    userService.insertUser(newUser,roleNewUser);
                    
                    Address newAddress = new Address(addressState,addressCity,Integer.parseInt(addressZip),
                    		addressStreet,Integer.parseInt(addressCivic));
                    addressService.insertAddress(newAddress, newUser.getIdUser());
                    response.sendRedirect("/rentalcardb/UserServlet?action=list");
                    break;
                case "delete":
                	userService.deleteUser(idUser);
                	response.sendRedirect("/rentalcardb/UserServlet?action=list");
                    break;
                case "edit":
                	User tempUser = userService.getUserById(Integer.parseInt(idUser));
                	String formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(tempUser.getDateOfBirth());
                	request.setAttribute("formattedDate", formattedDate);
                	request.setAttribute("user", userService.getUserById(Integer.parseInt(idUser)));
                	dispatcher = request.getRequestDispatcher("user-form.jsp");
                	dispatcher.forward(request, response);
                    break;
                case "update":
                	String name = request.getParameter("name");
            		String surname = request.getParameter("surname");
            		
            		String tempDate = request.getParameter("dateOfBirth");
            		Date dateOfBirth = new SimpleDateFormat("yyyy-MM-dd").parse(tempDate);          		
            		
            		String email = request.getParameter("email");
            		String password = request.getParameter("password");
                    userService.updateUser(idUser,name, surname, dateOfBirth,email, password);
                    session.removeAttribute("currentUser");
                    session.setAttribute("currentUser", userService.getUserByEmail(email));
                    response.sendRedirect("/rentalcardb/UserServlet?action=list");
                    break;
                case "logout":
                	session.removeAttribute("currentUser");
                	session.removeAttribute("roleUser");
                	dispatcher = request.getRequestDispatcher("login.jsp");
                	dispatcher.forward(request, response);
                	break;
                default:
                	request.setAttribute("listUser", userService.listUsers());
                	dispatcher = request.getRequestDispatcher("user-list.jsp");
                	dispatcher.forward(request, response);
                    break;
            }
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            try {
            	request.setAttribute("listUser", userService.listUsers());
            	dispatcher = request.getRequestDispatcher("user-list.jsp");
            	dispatcher.forward(request, response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
}
