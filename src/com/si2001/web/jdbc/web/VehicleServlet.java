package com.si2001.web.jdbc.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.RequestDispatcher;

import com.si2001.web.jdbc.model.Vehicle;
import com.si2001.web.jdbc.service.VehicleService;

/**
 * Servlet implementation class VehicleServlet
 */
@WebServlet("/VehicleServlet")
public class VehicleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private VehicleService vehicleService;

    public void init() {
        vehicleService = new VehicleService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String action = request.getParameter("action");
        String idVehicle = request.getParameter("idVehicle");
        RequestDispatcher dispatcher = null;
        
        try {
            switch (action) {
                case "new":
                	dispatcher = request.getRequestDispatcher("vehicle-form.jsp");
                	dispatcher.forward(request, response);
                	break;
                case "insert":
                	String newModel = request.getParameter("model");
                	
                	String newTempDate = request.getParameter("yearOfRegistration");
            		Date newYearOfRegistration = new SimpleDateFormat("yyyy-MM-dd").parse(newTempDate);
                
            		String newLicensePlate = request.getParameter("licensePlate");
            		String newIdCategory = request.getParameter("idCategory");
            		Vehicle newVehicle = new Vehicle(newModel, newYearOfRegistration, newLicensePlate);
                    vehicleService.insertVehicle(newVehicle, newIdCategory);
                    response.sendRedirect("/rentalcardb/VehicleServlet?action=list");
                    break;
                case "delete":
                	vehicleService.deleteVehicle(idVehicle);
                	response.sendRedirect("/rentalcardb/VehicleServlet?action=list");
                    break;
                case "edit":
                	Vehicle tempVehicle = vehicleService.getVehicleById(Integer.parseInt(idVehicle));
                	String formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(tempVehicle.getYearOfRegistration());
                	request.setAttribute("formattedDate", formattedDate);
                	request.setAttribute("vehicle", vehicleService.getVehicleById(Integer.parseInt(idVehicle)));
                	dispatcher = request.getRequestDispatcher("vehicle-form.jsp");
                	dispatcher.forward(request, response);
                    break;
                case "update":
                	String model = request.getParameter("model");
                	
                	String tempDate = request.getParameter("yearOfRegistration");
            		Date yearOfRegistration = new SimpleDateFormat("yyyy-MM-dd").parse(tempDate);
                	
            		String licensePlate = request.getParameter("licensePlate");
                    vehicleService.updateVehicle(idVehicle, model, yearOfRegistration, 
            				licensePlate);
                    response.sendRedirect("/rentalcardb/VehicleServlet?action=list");
                    break;
                default:
                	request.setAttribute("listVehicle", vehicleService.listVehicles());
                	dispatcher = request.getRequestDispatcher("vehicle-list.jsp");
                	dispatcher.forward(request, response);
                    break;
            }
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            try {
            	request.setAttribute("listVehicles", vehicleService.listVehicles());
            	dispatcher = request.getRequestDispatcher("vehicle-list.jsp");
            	dispatcher.forward(request, response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
}
