package com.si2001.web.jdbc.web;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.si2001.web.jdbc.model.Role;
import com.si2001.web.jdbc.service.RoleService;

/**
 * Servlet implementation class RoleServlet
 */
@WebServlet("/RoleServlet")
public class RoleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private RoleService roleService;

    public void init() {
    	roleService = new RoleService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String action = request.getParameter("action");
        String idRole = request.getParameter("idRole");
        RequestDispatcher dispatcher = null;
        
        try {
            switch (action) {
                case "new":
                	dispatcher = request.getRequestDispatcher("role-form.jsp");
                	dispatcher.forward(request, response);
                	break;
                case "insert":
                	String newDescription = request.getParameter("description");
            		Role newRole= new Role(newDescription);
                    roleService.insertRole(newRole);
                    response.sendRedirect("/rentalcardb/RoleServlet?action=list");
                    break;
                case "delete":
                	roleService.deleteRole(idRole);
                	response.sendRedirect("/rentalcardb/RoleServlet?action=list");
                    break;
                case "edit":
                	request.setAttribute("role", roleService.showEditForm(Integer.parseInt(idRole)));
                	dispatcher = request.getRequestDispatcher("role-form.jsp");
                	dispatcher.forward(request, response);
                    break;
                case "update":
                	String description = request.getParameter("description");
            		Role role = new Role(Integer.parseInt(idRole), description);
                	roleService.updateRole(role);
                	response.sendRedirect("/rentalcardb/RoleServlet?action=list");
                    break;
                default:
                	request.setAttribute("listRole", roleService.listRoles());
                	dispatcher = request.getRequestDispatcher("role-list.jsp");
                	dispatcher.forward(request, response);
                    break;
            }
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            try {
            	roleService.listRoles();
            	dispatcher = request.getRequestDispatcher("role-list.jsp");
            	dispatcher.forward(request, response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
}
