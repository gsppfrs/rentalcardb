package com.si2001.web.jdbc.web;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.si2001.web.jdbc.model.Address;
import com.si2001.web.jdbc.service.AddressService;

/**
 * Servlet implementation class AddressServlet
 */
@WebServlet("/AddressServlet")
public class AddressServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private AddressService addressService;

    public void init() {
        addressService = new AddressService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String action = request.getParameter("action");
        String idAddress = request.getParameter("idAddress");
        RequestDispatcher dispatcher = null;
        
        try {
            switch (action) {
                case "new":
                	dispatcher = request.getRequestDispatcher("address-form.jsp");
                	dispatcher.forward(request, response);
                	break;
                case "insert":
                	String newState = request.getParameter("state");
            		String newCity = request.getParameter("city");
            		Integer newZip = Integer.parseInt(request.getParameter("zip"));
            		String newStreetName = request.getParameter("streetName");
            		Integer newCivicNumber = Integer.parseInt(request.getParameter("civicNumber"));
            		Address newAddress = new Address(newState, newCity, newZip, newStreetName, newCivicNumber);
                    //addressService.insertAddress(newAddress);
                    response.sendRedirect("/rentalcardb/AddressServlet?action=list");
                    break;
                case "delete":
                	addressService.deleteAddress(idAddress);
                	response.sendRedirect("/rentalcardb/AddressServlet?action=list");
                    break;
                case "edit":
                	request.setAttribute("address", addressService.getAddressById(Integer.parseInt(idAddress)));
                	dispatcher = request.getRequestDispatcher("address-form.jsp");
                	dispatcher.forward(request, response);
                    break;
                case "update":
            		String state = request.getParameter("state");
            		String city = request.getParameter("city");
            		Integer zip = Integer.parseInt(request.getParameter("zip"));
            		String streetName = request.getParameter("streetName");
            		Integer civicNumber = Integer.parseInt(request.getParameter("civicNumber"));
            		Address address = new Address(Integer.parseInt(idAddress), state, city, zip, streetName, civicNumber);
                	addressService.updateAddress(address);
                	response.sendRedirect("/rentalcardb/AddressServlet?action=list");
                    break;
                default:
                	request.setAttribute("listAddress", addressService.listAddress());
                	dispatcher = request.getRequestDispatcher("address-list.jsp");
                	dispatcher.forward(request, response);
                    break;
            }
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            try {
            	addressService.listAddress();
            	dispatcher = request.getRequestDispatcher("address-list.jsp");
            	dispatcher.forward(request, response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
}
