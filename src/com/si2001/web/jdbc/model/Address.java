package com.si2001.web.jdbc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "address")
public class Address {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idAddress")
	protected Integer idAddress;
	
	@Column(name = "state")
	protected String state;
	
	@Column(name = "city")
	protected String city;
	
	@Column(name = "zip")
	protected Integer zip;
	
	@Column(name = "streetName")
	protected String streetName;
	
	@Column(name = "civicNumber")
	protected Integer civicNumber;
	
	
	@ManyToOne
	@JoinColumn(name = "idUser",referencedColumnName = "idUser")
	private User user;
	
	public Address() {
	}
	public Address(Integer idAddress, String state, String city, Integer zip, String streetName, Integer civicNumber) {
		super();
		this.idAddress = idAddress;
		this.state = state;
		this.city = city;
		this.zip = zip;
		this.streetName = streetName;
		this.civicNumber = civicNumber;
	}
	public Address(String state, String city, Integer zip, String streetName, Integer civicNumber) {
		super();
		this.state = state;
		this.city = city;
		this.zip = zip;
		this.streetName = streetName;
		this.civicNumber = civicNumber;
	}
	public Integer getIdAddress() {
		return idAddress;
	}
	public void setIdAddress(Integer idAddress) {
		this.idAddress = idAddress;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Integer getZip() {
		return zip;
	}
	public void setZip(Integer zip) {
		this.zip = zip;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public Integer getCivicNumber() {
		return civicNumber;
	}
	public void setCivicNumber(Integer civicNumber) {
		this.civicNumber = civicNumber;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Address [idAddress=" + idAddress + ", state=" + state + ", city=" + city + ", zip=" + zip
				+ ", streetName=" + streetName + ", civicNumber=" + civicNumber + ", user=" + user + "]";
	}
	
}
