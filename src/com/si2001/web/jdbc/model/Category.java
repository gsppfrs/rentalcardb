package com.si2001.web.jdbc.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "category")
public class Category {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idCategory")
	private Integer idCategory;
	
	@Column(name = "description")
	private String description;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "category")
	private Set<Vehicle> vehicle = new HashSet<>();
	
	public Category() {
	}
	public Set<Vehicle> getVehicles() {
		return vehicle;
	}
	public void setVehicles(Set<Vehicle> vehicles) {
		this.vehicle = vehicles;
	}
	public Category(Integer idCategory, String description) {
		super();
		this.idCategory = idCategory;
		this.description = description;
	}
	public Category(String description) {
		super();
		this.description = description;
	}
	public Integer getIdCategory() {
		return idCategory;
	}
	public void setIdCategory(Integer idCategory) {
		this.idCategory = idCategory;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "Category [idCategory=" + idCategory + ", description=" + description + "]";
	}
	
}
