package com.si2001.web.jdbc.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "vehicle")
public class Vehicle {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idVehicle")
	private Integer idVehicle;
	
	@Column(name = "model")
	private String model;
	
	@Column(name = "yearOfRegistration")
	private Date yearOfRegistration;
	
	@Column(name = "licensePlate")
	private String licensePlate;
	
	@ManyToOne
	@JoinColumn(name = "idCategory",referencedColumnName = "idCategory")
	private Category category;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "vehicle")
	private Set<Rental> rental = new HashSet<>();
	
	public Vehicle() {
	}
	public Vehicle(Integer idVehicle, String model, Date yearOfRegistration, String licensePlate) {
		super();
		this.idVehicle = idVehicle;
		this.model = model;
		this.yearOfRegistration = yearOfRegistration;
		this.licensePlate = licensePlate;
	}
	public Vehicle(String model, Date yearOfRegistration, String licensePlate) {
		super();
		this.model = model;
		this.yearOfRegistration = yearOfRegistration;
		this.licensePlate = licensePlate;
	}
	public Integer getIdVehicle() {
		return idVehicle;
	}
	public void setIdVehicle(int idVehicle) {
		this.idVehicle = idVehicle;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public Date getYearOfRegistration() {
		return yearOfRegistration;
	}
	public void setYearOfRegistration(Date yearOfRegistration) {
		this.yearOfRegistration = yearOfRegistration;
	}
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public Set<Rental> getRental() {
		return rental;
	}
	public void setRental(Set<Rental> rental) {
		this.rental = rental;
	}
	public void setIdVehicle(Integer idVehicle) {
		this.idVehicle = idVehicle;
	}
	@Override
	public String toString() {
		return "Vehicle [idVehicle=" + idVehicle + ", model=" + model + ", yearOfRegistration=" + yearOfRegistration
				+ ", licensePlate=" + licensePlate + ", category=" + category + ", rental=" + rental + "]";
	}
	
	
}
