package com.si2001.web.jdbc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "rental")
public class Rental {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idRental")
	private Integer idRental;
	
	@Column(name = "rentalStartDate")
	private Date rentalStartDate;
	
	@Column(name = "rentalEndDate")
	private Date rentalEndDate;
	
	@Column(name = "validate")
	private Integer validate;
	
	@ManyToOne
	@JoinColumn(name = "idVehicle",referencedColumnName = "idVehicle")
	private Vehicle vehicle;
	
	@ManyToOne
	@JoinColumn(name = "idUser",referencedColumnName = "idUser")
	private User user;
	
	public Rental() {
	}
	
	public Rental(Integer idRental, Date rentalStartDate, Date rentalEndDate, Integer validate) {
		super();
		this.idRental = idRental;
		this.rentalStartDate = rentalStartDate;
		this.rentalEndDate = rentalEndDate;
		this.validate = validate;
	}
	public Rental(Date newRentalStartDate, Date newRentalEndDate, Integer validate) {
		super();
		this.rentalStartDate = newRentalStartDate;
		this.rentalEndDate = newRentalEndDate;
		this.validate = validate;
	}
	
	
	public Rental(Integer idRental, Date rentalStartDate, Date rentalEndDate, Integer validate, Vehicle vehicle,
			User user) {
		super();
		this.idRental = idRental;
		this.rentalStartDate = rentalStartDate;
		this.rentalEndDate = rentalEndDate;
		this.validate = validate;
		this.vehicle = vehicle;
		this.user = user;
	}

	public Integer getIdRental() {
		return idRental;
	}
	public void setIdRental(Integer idRental) {
		this.idRental = idRental;
	}
	public Date getRentalStartDate() {
		return rentalStartDate;
	}
	public void setRentalStartDate(Date rentalStartDate) {
		this.rentalStartDate = rentalStartDate;
	}
	public Date getRentalEndDate() {
		return rentalEndDate;
	}
	public void setRentalEndDate(Date rentalEndDate) {
		this.rentalEndDate = rentalEndDate;
	}
	public Integer getValidate() {
		return validate;
	}
	public void setValidate(Integer validate) {
		this.validate = validate;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Rental [idRental=" + idRental + ", rentalStartDate=" + rentalStartDate + ", rentalEndDate="
				+ rentalEndDate + ", validate=" + validate + ", vehicle=" + vehicle + ", user=" + user + "]";
	}
	
	
}
