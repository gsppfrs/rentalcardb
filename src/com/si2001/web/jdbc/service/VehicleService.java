package com.si2001.web.jdbc.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import com.si2001.web.jdbc.dao.CategoryDao;
import com.si2001.web.jdbc.dao.VehicleDao;
import com.si2001.web.jdbc.model.Category;
import com.si2001.web.jdbc.model.Vehicle;

public class VehicleService {

VehicleDao vehicleDao;
	
	public VehicleService() {
		super();
		this.vehicleDao = new VehicleDao();
	}

	public List<Vehicle> listVehicles()
			throws SQLException, IOException, ServletException {
		return vehicleDao.getAllVehicles();
	}

	public Vehicle getVehicleById(Integer idVehicle)
			throws SQLException, ServletException, IOException {
		return vehicleDao.getVehicle(idVehicle);
	}

	public void insertVehicle(Vehicle newVehicle, String newIdCategory)
			throws SQLException, IOException {
		CategoryDao categoryDao = new CategoryDao();
        Category tempCategory = categoryDao.getCategory(Integer.parseInt(newIdCategory));
        newVehicle.setCategory(tempCategory);
        Set<Vehicle> temSet = tempCategory.getVehicles();
        temSet.add(newVehicle);
        tempCategory.setVehicles(temSet);
		vehicleDao.saveVehicle(newVehicle);
	}
	
	public void updateVehicle(String idVehicle ,String model, Date yearOfRegistration, String licensePlate) {
		// TODO Auto-generated method stub
		Vehicle tempVehicle = new Vehicle(Integer.parseInt(idVehicle), model, yearOfRegistration, licensePlate);
		vehicleDao.updateVehicle(tempVehicle);
	}

	public void deleteVehicle(String idVehicle)
			throws SQLException, IOException {
		vehicleDao.deleteVehicle(Integer.parseInt(idVehicle));
	}

	

}


