package com.si2001.web.jdbc.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;

import com.si2001.web.jdbc.dao.RoleDao;
import com.si2001.web.jdbc.model.Role;

public class RoleService {

RoleDao roleDao;
	
	public RoleService() {
		super();
		this.roleDao = new RoleDao();
	}

	public List<Role> listRoles()
			throws SQLException, IOException, ServletException {
		return roleDao.getAllRoles();
	}

	public Role showEditForm(Integer idRole)
			throws SQLException, ServletException, IOException {
		return roleDao.getRole(idRole);
		
	}

	public void insertRole(Role newRole)
			throws SQLException, IOException {
		roleDao.saveRole(newRole);
	}

	public void updateRole(Role role)
			throws SQLException, IOException {
		roleDao.updateRole(role);
	}

	public void deleteRole(String idRole)
			throws SQLException, IOException {
		roleDao.deleteRole(Integer.parseInt(idRole));
	}

}
