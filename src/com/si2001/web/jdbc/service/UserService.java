package com.si2001.web.jdbc.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import com.si2001.web.jdbc.dao.AddressDao;
import com.si2001.web.jdbc.dao.RoleDao;
import com.si2001.web.jdbc.dao.UserDao;
import com.si2001.web.jdbc.model.Role;
import com.si2001.web.jdbc.model.User;

public class UserService {

UserDao userDao;
RoleDao roleDao;
AddressDao addressDao;
	
	public UserService() {
		super();
		this.userDao = new UserDao();
		this.roleDao = new RoleDao();
		this.addressDao = new AddressDao();
	}

	public List<User> listUsers()
			throws SQLException, IOException, ServletException {
		return userDao.getAllUsers();
	}

	public User getUserById(Integer idUser)
			throws SQLException, ServletException, IOException {
		return userDao.getUser(idUser);
		
	}

	public void insertUser(User newUser, String role)
			throws SQLException, IOException {
		
		Role tempRole = roleDao.getRole(Integer.parseInt(role));
		Set<Role> roles = new HashSet<>();
		roles.add(tempRole);
		newUser.setRole(roles);
		
		userDao.saveUser(newUser);
	}
	
	public void updateUser(String idUser, String name, String surname, Date dateOfBirth, String email,
			String password) {
		// TODO Auto-generated method stub
		User tempUser = new User(Integer.parseInt(idUser),name,surname,dateOfBirth,email,password);
		userDao.updateUser(tempUser);
	}
	
	//Vecchio metodo updateUser. Creava l'utente nella servlet non nel service.
	/*
	public void updateUser(User user)
			throws SQLException, IOException {
		userDao.updateUser(user);
	}
	*/

	public void deleteUser(String idUser)
			throws SQLException, IOException {
		userDao.deleteUser(Integer.parseInt(idUser));
	}

	public static boolean checkLogin(String loginEmail, String loginPassword) {
		return UserDao.isValid(loginEmail, loginPassword);
	}
	
	//get user by email
	public User getUserByEmail(String emailUser)
			throws SQLException, IOException, ServletException {
		return userDao.getUserByEmail(emailUser);
	}
}

