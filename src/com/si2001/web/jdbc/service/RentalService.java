package com.si2001.web.jdbc.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import com.si2001.web.jdbc.dao.RentalDao;
import com.si2001.web.jdbc.dao.UserDao;
import com.si2001.web.jdbc.dao.VehicleDao;
import com.si2001.web.jdbc.model.Rental;
import com.si2001.web.jdbc.model.User;
import com.si2001.web.jdbc.model.Vehicle;

public class RentalService {

RentalDao rentalDao;
UserDao userDao;
VehicleDao vehicleDao;
	
	public RentalService() {
		super();
		this.rentalDao = new RentalDao();
		this.userDao = new UserDao();
		this.vehicleDao = new VehicleDao();
	}

	public List<Rental> listRentals()
			throws SQLException, IOException, ServletException {
		return rentalDao.getAllRentals();
	}

	public Rental getRentalById(Integer idRental)
			throws SQLException, ServletException, IOException {
		return rentalDao.getRental(idRental);
		
	}

	public void insertRental(Rental newRental, String idVehicle, String idUser)
			throws SQLException, IOException {
		
		UserDao userDao = new UserDao();
		boolean isEmpy = false;
		User tempUser = userDao.getUser(Integer.parseInt(idUser));
		VehicleDao vehicleDao = new VehicleDao();
		Vehicle tempVehicle = vehicleDao.getVehicle(Integer.parseInt(idVehicle));
		
		newRental.setUser(tempUser);
		newRental.setVehicle(tempVehicle);
		
		Set<Rental> tempRentals = tempVehicle.getRental();
		tempRentals.add(newRental);
		
		tempVehicle.setRental(tempRentals);
		tempUser.setRental(tempRentals);
		
		
		if(newRental.getRentalStartDate().compareTo(newRental.getRentalEndDate())==0) {
			isEmpy = true;
		}else if (newRental.getRentalStartDate().compareTo(newRental.getRentalEndDate())<0) {
			isEmpy = true;
		}
		if(rentalDao.getAllRentals()!= null) {
			for(Rental tempRental : rentalDao.checkDate(newRental)) {
				// Start date of new rent
				Date start = newRental.getRentalStartDate();
				// End date of new rent
				Date end = newRental.getRentalEndDate();
				// Start date of current tempRental element
				Date tempStart = tempRental.getRentalStartDate();
				// End date of current tempRental element
				Date tempEnd = tempRental.getRentalEndDate();
			
			if( ( end.after(tempStart) || end.equals(tempStart) ) && ( end.before(tempEnd ) || end.equals(tempEnd) ) ) {
				System.out.println("Processing only end in range");
				isEmpy = false;
				break;
			}
			if( ( start.after(tempStart) || start.equals(tempStart) ) && ( start.before(tempEnd ) || start.equals(tempEnd) ) ) {
				System.out.println("Processing only start in range");
				isEmpy = false;
				break;
			}
		}
		
		if(isEmpy)
			{
				rentalDao.saveRental(newRental);
			}else {
				System.out.print("Errore");
			}
		}
	}
	public void deleteRental(String idRental)
			throws SQLException, IOException {
		rentalDao.deleteRental(Integer.parseInt(idRental));
	}

	public void updateRental(String idRental, Date rentalStartDate, Date rentalEndDate, Integer validate,
			String idUserString, String idVehicleString) {
		// TODO Auto-generated method stub
		Rental rental = new Rental(Integer.parseInt(idRental), rentalStartDate, rentalEndDate, validate,
				vehicleDao.getVehicle(Integer.parseInt(idVehicleString)), userDao.getUser(Integer.parseInt(idUserString)));
		rentalDao.updateRental(rental);
	}

}

