package com.si2001.web.jdbc.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import com.si2001.web.jdbc.model.Address;
import com.si2001.web.jdbc.model.User;
import com.si2001.web.jdbc.dao.AddressDao;
import com.si2001.web.jdbc.dao.UserDao;

public class AddressService {
	AddressDao addressDao;
	
	public AddressService() {
		super();
		this.addressDao = new AddressDao();
	}

	public List<Address> listAddress()
			throws SQLException, IOException, ServletException {
		return addressDao.getAllAddress();
	}

	public Address getAddressById(Integer idAddress)
			throws SQLException, ServletException, IOException {
		return addressDao.getAddress(idAddress);
		
	}
	
	public Address getAddressWithoutId(String state, String city, Integer zip, String streetName, Integer civicNumber) {
		return addressDao.getAddressWithOutId(state, city, zip, streetName, civicNumber);
	}

	public void insertAddress(Address newAddress, Integer idUser)
			throws SQLException, IOException {
		
		UserDao userDao = new UserDao();
		User tempUser = userDao.getUser(idUser);
		
		newAddress.setUser(tempUser);
		
		Set<Address> tempAddresses = tempUser.getAddress();
		tempAddresses.add(newAddress);
		
		tempUser.setAddress(tempAddresses);
		
		addressDao.saveAddress(newAddress);
	}

	public void updateAddress(Address address)
			throws SQLException, IOException {
		addressDao.updateAddress(address);
	}

	public void deleteAddress(String idAddress)
			throws SQLException, IOException {
		addressDao.deleteAddress(Integer.parseInt(idAddress));
	}

}
