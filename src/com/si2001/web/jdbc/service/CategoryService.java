package com.si2001.web.jdbc.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;

import com.si2001.web.jdbc.dao.CategoryDao;
import com.si2001.web.jdbc.model.Category;

public class CategoryService {

	CategoryDao categoryDao;
	
	public CategoryService() {
		super();
		this.categoryDao = new CategoryDao();
	}

	public List<Category> listCategories()
			throws SQLException, IOException, ServletException {
		return categoryDao.getAllCategory();
	}

	public Category showEditForm(Integer idCategory)
			throws SQLException, ServletException, IOException {
		return categoryDao.getCategory(idCategory);
		
	}

	public void insertCategory(Category newCategory)
			throws SQLException, IOException {
		categoryDao.saveCategory(newCategory);
	}

	public void updateCategory(Category category)
			throws SQLException, IOException {
		categoryDao.updateCategory(category);
	}

	public void deleteCategory(String idCategory)
			throws SQLException, IOException {
		categoryDao.deleteCategory(Integer.parseInt(idCategory));
	}

}
