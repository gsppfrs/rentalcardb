package com.si2001.web.jdbc.util;

import java.util.Properties;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import com.si2001.web.jdbc.model.Address;
import com.si2001.web.jdbc.model.Category;
import com.si2001.web.jdbc.model.User;
import com.si2001.web.jdbc.model.Rental;
import com.si2001.web.jdbc.model.Role;
import com.si2001.web.jdbc.model.Vehicle;

public class HibernateUtil {
	 private static SessionFactory sessionFactory;

	 public static SessionFactory getSessionFactory() {
	  if (sessionFactory == null) {
	   try {
	    Configuration configuration = new Configuration();

	    // Hibernate settings equivalent to hibernate.cfg.xml's properties
	    Properties settings = new Properties();
	    settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
	    settings.put(Environment.URL, "jdbc:mysql://localhost:3306/rentalcardb?useSSL=FALSE");
	    settings.put(Environment.USER, "root");
	    settings.put(Environment.PASS, "1234");
	    settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
	    settings.put(Environment.JDBC_TIME_ZONE,"+01:00");
	    
	    //settings.put(Environment.DRIVER, "");

	    settings.put(Environment.SHOW_SQL, "true");

	    settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");

	    settings.put(Environment.HBM2DDL_AUTO, "none");

	    configuration.setProperties(settings);
	    
	    configuration.addAnnotatedClass(Category.class);
	    configuration.addAnnotatedClass(Address.class);
	    configuration.addAnnotatedClass(User.class);
	    configuration.addAnnotatedClass(Rental.class);
	    configuration.addAnnotatedClass(Role.class);
	    configuration.addAnnotatedClass(Vehicle.class);
	    

	    ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
	      .applySettings(configuration.getProperties()).build();
	    
	    System.out.println("Hibernate Java Config service Registry created");
	    
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	    return sessionFactory;

	   } catch (Exception e) {
	    e.printStackTrace();
	   }
	  }
	  return sessionFactory;
	 }
	}